const { test, expect } = require('@playwright/test');
const LoginPage = require('../page-objects/LoginPage');
const InventoryPage = require('../page-objects/InventoryPage');

test.describe('SauceDemo Sorting Test', () => {
  let loginPage;
  let inventoryPage;

  test.beforeEach(async ({ page }) => {
    loginPage = new LoginPage(page);
    inventoryPage = new InventoryPage(page);
  });

  test('should sort items correctly', async ({ page, baseURL }) => {
    // Load login credentials
    const credentials = {
      username: 'standard_user',
      password: 'secret_sauce'
    };

    // Visit the SauceDemo login page using the baseURL
    await page.goto(baseURL);

    // Log in using credentials from the fixture
    await loginPage.enterUsername(credentials.username);
    await loginPage.enterPassword(credentials.password);
    await loginPage.submit();

    // Verify items are sorted by Name (A -> Z)
    const itemNames = await inventoryPage.getItems();
    const sortedNames = [...itemNames].sort();
    expect(itemNames).toEqual(sortedNames);

    // Verify the default sorting option is Name (A -> Z)
    const sortingOptionValue = await inventoryPage.getSortingOptionValue();
    expect(sortingOptionValue).toBe('az');

    const sortingOptionText = await inventoryPage.getSortingOptionText();
    expect(sortingOptionText).toBe('Name (A to Z)');

    // Change sorting to Name (Z -> A)
    await inventoryPage.changeSortingToZA();

    // Verify items are sorted by Name (Z -> A)
    const itemNamesZA = await inventoryPage.getItems();
    const sortedNamesZA = [...itemNamesZA].sort().reverse();
    expect(itemNamesZA).toEqual(sortedNamesZA);

    // Verify the selected sorting option is Name (Z -> A)
    const sortingOptionValueZA = await inventoryPage.getSortingOptionValue();
    expect(sortingOptionValueZA).toBe('za');

    const sortingOptionTextZA = await inventoryPage.getSortingOptionText();
    expect(sortingOptionTextZA).toBe('Name (Z to A)');
  });
});
