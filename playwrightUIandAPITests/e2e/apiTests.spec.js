const { test, expect, request } = require('@playwright/test');

test.describe('JSONPlaceholder API Tests', () => {
  let initialPostCount;
  let newPostId;

  test.beforeAll(async () => {
    // Send a GET request to retrieve all posts and store the total count
    const context = await request.newContext();
    const response = await context.get('https://jsonplaceholder.typicode.com/posts');
    expect(response.ok()).toBeTruthy();
    const posts = await response.json();
    initialPostCount = posts.length;
  });

  test('Create a new post', async ({ request }) => {
    // Send a POST request to create a new post
    const newPost = {
      title: 'Michael',
      body: 'Body of life',
      userId: 1000,
    };
    const response = await request.post('https://jsonplaceholder.typicode.com/posts', { data: newPost });
    expect(response.ok()).toBeTruthy();
    const responseBody = await response.json();
    expect(responseBody).toHaveProperty('id');
    newPostId = responseBody.id;
  });

  test('Get the created post by ID', async ({ request }) => {
    // Send a GET request to retrieve the created post by ID, this is bound to fail as the creation is just a mock and the new post doesnt actually exist in the syatem
    const response = await request.get(`https://jsonplaceholder.typicode.com/posts/${newPostId}`);
    expect(response.ok()).toBeTruthy();
    const post = await response.json();
    expect(post).toHaveProperty('id', newPostId);
    expect(post).toHaveProperty('title', 'Michael');
    expect(post).toHaveProperty('body', 'Body of life');
    expect(post).toHaveProperty('userId', 1000);
  });

  test('Update the created post with PATCH', async ({ request }) => {
    // Send a PATCH request to update the created post
    const updatedPost = {
      title: 'Michael Adeoye',
    };
    const response = await request.patch(`https://jsonplaceholder.typicode.com/posts/${newPostId}`, { data: updatedPost });
    // use the PATCH rensponse to verify the update (not ideal)
    expect(response.ok()).toBeTruthy();
    const responseBody = await response.json();
    expect(responseBody).toHaveProperty('title', 'Michael Adeoye');
  });

    // Verify the update  
   test('Verify the created post by GETting it', async ({ request }) => { 
    //this part is bound to fail because the created post was just a mock and getting the id back would not retrive that newly created post
    const getResponse = await request.get(`https://jsonplaceholder.typicode.com/posts/${newPostId}`);
    expect(getResponse.ok()).toBeTruthy();
    const post = await getResponse.json();
    expect(post).toHaveProperty('title', 'Michael Adeoye');
  });

  test('Delete the created post by ID', async ({ request }) => {
    // Send a DELETE request to delete the created post
    const response = await request.delete(`https://jsonplaceholder.typicode.com/posts/${newPostId}`);
    expect(response.ok()).toBeTruthy();

    // Verify the deletion 
    const getResponse = await request.get(`https://jsonplaceholder.typicode.com/posts/${newPostId}`);
    expect(getResponse.status()).toBe(404);
    // this will give a false pass as nothin was actuallly created in the first place
  })

  test('Check the number of posts to ensure integrity', async ({ request }) => {
    // Send a GET request to retrieve all posts and compare the count
    const response = await request.get('https://jsonplaceholder.typicode.com/posts');
    expect(response.ok()).toBeTruthy();
    const posts = await response.json();
    const currentPostCount = posts.length;
    expect(currentPostCount).toBe(initialPostCount);    // this will give a false pass as nothin was actuallly created or deleted, it was all just mocked
  });
});
