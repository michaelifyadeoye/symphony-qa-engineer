class InventoryPage {
    constructor(page) {
      this.page = page;
      this.items = page.locator('.inventory_item_name');
      this.sortingDropdown = page.locator('[data-test="product-sort-container"]');
      this.selectedSortingOption = page.locator('[data-test="product-sort-container"] option:checked');
    }
  
    async getItems() {
      return await this.items.allTextContents();
    }
  
    async changeSortingToZA() {
      await this.sortingDropdown.selectOption('za');
    }
  
    async getSortingOptionValue() {
      return await this.sortingDropdown.inputValue();
    }
  
    async getSortingOptionText() {
      return await this.selectedSortingOption.textContent();
    }
  }
  
  module.exports = InventoryPage;
  