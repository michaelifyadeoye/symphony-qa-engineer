class LoginPage {
    constructor(page) {
      this.page = page;
      this.usernameField = page.locator('[data-test="username"]');
      this.passwordField = page.locator('[data-test="password"]');
      this.loginButton = page.locator('[data-test="login-button"]');
    }
  
    async enterUsername(username) {
      await this.usernameField.fill(username);
    }
  
    async enterPassword(password) {
      await this.passwordField.fill(password);
    }
  
    async submit() {
      await this.loginButton.click();
    }
  }
  
  module.exports = LoginPage;
  