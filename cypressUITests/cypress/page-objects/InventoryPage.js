class InventoryPage {
    getItems() {
      return cy.get('.inventory_item_name');
    }
  
    changeSortingToZA() {
      cy.get('[data-test="product-sort-container"]').select('za');
    }
  
    getSortingOption() {
      return cy.get('[data-test="product-sort-container"]');
    }
  }
  
  export default InventoryPage;
  