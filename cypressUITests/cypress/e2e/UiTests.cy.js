import LoginPage from '../page-objects/LoginPage';
import InventoryPage from '../page-objects/InventoryPage';

const loginPage = new LoginPage();
const inventoryPage = new InventoryPage();

describe('Saucedemo Sorting Test(symphony)', () => {
  before(() => {
    cy.fixture('credentials').as('creds');
  });

  it('should sort items correctly', function() {
    // Visit the SauceDemo website
    cy.visit("/");
    
    // Log in using credentials from the fixture
    loginPage.enterUsername(this.creds.username);
    loginPage.enterPassword(this.creds.password);
    loginPage.submit();
    
    // Verify items are sorted by Name (A -> Z)
    inventoryPage.getItems().then(items => {
      const itemNames = [...items].map(item => item.innerText);
      const sortedNames = [...itemNames].sort();
      expect(itemNames).to.deep.equal(sortedNames);
    });

    // Verify the default sorting option is Name (A -> Z)
    inventoryPage.getSortingOption()
    .should('have.value', 'az')
    .should('contain.text', 'Name (A to Z)');
    
    // Change sorting to Name (Z -> A)
    inventoryPage.changeSortingToZA();
    
    // Verify items are sorted by Name (Z -> A)
    inventoryPage.getItems().then(items => {
      const itemNames = [...items].map(item => item.innerText);
      const sortedNames = [...itemNames].sort().reverse();
      expect(itemNames).to.deep.equal(sortedNames);
    });

    // Verify the selected sorting option is Name (Z -> A)
    inventoryPage.getSortingOption()
    .should('have.value', 'za')
    .should('contain.text', 'Name (Z to A)');
  });
});
