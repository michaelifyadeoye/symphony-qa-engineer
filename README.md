# Symphony QA Engineer 



This project contains automated UI tests for the SauceDemo website and api tests for https://jsonplaceholder.typicode.com/ using Cypress and Plawright

## Running Tests Locally with cypress

1. Clone the repository:
   ```bash
   git clone <your-repo-url>
   cd cypressUITests
   ```

2. Install dependencies:
   ```bash
   npm install
   ```

3. Run tests in interactive mode:
   ```bash
   npm run cypress:open
   ```

4. Run tests in headless mode:
   ```bash
   npm run cypress:run
   ```

## Running Tests Locally with Playwright

1. Clone the repository:
   ```bash
   git clone <your-repo-url>
   cd playwrightUIandAPITests
   ```

2. Install dependencies:
   ```bash
   npm install
   ```

3. Run tests in headed mode:
   ```bash
   npx playwright test --headed
   ```

4. Run tests in headless mode:
   ```bash
   npx playwright test
   ```

## Running Tests on GitLab CI/CD cypress and playwright

   The tests are automatically run in the GitLab CI/CD pipeline defined in the `.gitlab-ci.yml` file.

## Project Structure

- `cypressUITests/cypress/e2e/UiTests.cy.js`: Test file for SauceDemo cypress.
- `playwrightUIandAPITests/e2e/uiTests.spec.js`: Test file for SauceDemo playwright.
- `playwrightUIandAPITests/e2e/apiTests.spec.js`: Test file for jsonplaceholder API Plawright.
- `.gitlab-ci.yml`: CI/CD configuration file.

## Notes
   Ensure you have a GitLab account and a repository set up. The `.gitlab-ci.yml` file configures the CI/CD pipeline to run the tests on each push to the main branch.
